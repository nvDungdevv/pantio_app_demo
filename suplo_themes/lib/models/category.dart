class Category {
  int? id;
  String? nameOfCategory;
  String? imageUrl;
  Category({required this.id,required this.imageUrl,required this.nameOfCategory});
}
List<Category> FAKE_DATA_CATEGORIES=[
  Category(id: 1,nameOfCategory: "Áo", imageUrl: "assets/images/categories/ao.png"),
  Category(id: 2,nameOfCategory: "CHÂN VÁY", imageUrl: "assets/images/categories/chanVay.png"),
  Category(id: 3,nameOfCategory: "ĐẦM CÔNG SỞ ", imageUrl: "assets/images/categories/damCongSo.png"),
  Category(id: 4,nameOfCategory: "QUẦN", imageUrl: "assets/images/categories/quan.png"),
  Category(id: 5,nameOfCategory: "ĐẦM DẠO PHỐ", imageUrl: "assets/images/categories/damDaoPho.png"),
  Category(id: 6,nameOfCategory: "TÚI", imageUrl: "assets/images/categories/tui.png")
];