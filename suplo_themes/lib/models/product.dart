class Product{
  int? id;
  int? categoryId;
  String? productName;
  String? imageUrl;
  int? price;
  int? salePrice;

  Product({required this.id, required this.categoryId,required this.imageUrl, required this.productName, required this.price, required this.salePrice});

}
List<Product> FAKE_DATA_PRODUCT=[
  Product(id: 1, categoryId: 5, productName: "ÁO SƠ MI KIỂU FAK711 A32",imageUrl: "assets/images/products/ao1.png",price: 135000,salePrice: 100000 ),
  Product(id: 2, categoryId: 3, productName: "ÁO SƠ MI Khoác FAK711 A32",imageUrl: "assets/images/products/ao2.jpg",price: 135000, salePrice: 100000 ),
  Product(id: 3, categoryId: 2, productName: "VAY DÀI FAK711 A32",imageUrl: "assets/images/products/ao3.png",price: 135000, salePrice: 100000),

];