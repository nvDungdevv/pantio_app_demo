
import 'package:flutter/cupertino.dart';
import 'package:suplo_themes/screens/collection/collection_page.dart';
import 'package:suplo_themes/screens/collection/detail_collection_page.dart';
import 'package:suplo_themes/screens/home_page.dart';
import 'package:suplo_themes/screens/login/register_page.dart';
import 'package:suplo_themes/screens/product/list_product_page.dart';
import 'package:suplo_themes/screens/product_detail_page.dart';

final Map<String, WidgetBuilder > routers = {
  ProductDetailPage.routeName: (context) =>const ProductDetailPage(),
  CollectionPage.routeName: (context) =>const CollectionPage(),
  DetailCollectionPage.routeName: (context)=> const DetailCollectionPage(),
  ListProductPage.routeName: (context)=>  const ListProductPage(),
  RegisterPage.routeName:(context)=> const RegisterPage(),
  HomePage.routeName: (context)=> const HomePage(),
};