
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:suplo_themes/widgets/box_size_product.dart';
import 'package:suplo_themes/widgets/cart_icon.dart';
import 'package:suplo_themes/widgets/favorite_button.dart';
import 'package:suplo_themes/widgets/list_product.dart';

class ProductDetailPage extends StatefulWidget {
  const ProductDetailPage({Key? key}) : super(key: key);
  static String routeName ="/detailProduct";

  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        actions: [
          IconButton(onPressed: (){}, icon: const Icon(Icons.error_outline_sharp,size: 30,color:  Color.fromRGBO( 51, 51, 51,0.5))),
          FavoriteButton(),
          const CartIcon(),
        ],
        leading: IconButton(onPressed: (){
          Navigator.pop(context);
        }, icon: const Icon(Icons.keyboard_backspace,size: 30,color: Colors.black)),
      ),
      body: Stack(
        children: [
          Container(
            width:  MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height  ,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/products/ao4.png"),
                fit: BoxFit.cover,
              )
            ),
          ),
           DraggableScrollableSheet(
            initialChildSize: 0.4,
              minChildSize: 0.4,
              maxChildSize: 1.0,
              builder: (BuildContext context, ScrollController scrollController){
                return SingleChildScrollView(
                  controller: scrollController,
                  child: Container(
                            color: Colors.white,
                            width: MediaQuery.of(context).size.width,
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              children: [
                                Row(

                                  children: [
                                    IconButton(onPressed: (){},
                                        icon: Image.asset('assets/icons/barcode 1.png')
                                    ),
                                    IconButton(onPressed: (){},
                                        icon: Image.asset('assets/icons/share-2.png')
                                    ),
                                    const Spacer(),
                                    IconButton(onPressed: (){},
                                        icon: Image.asset('assets/icons/x.png')
                                    )
                                  ],
                                ),
                                Padding(padding: const EdgeInsets.only(bottom: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: const [
                                      Text("180.000 đ",
                                          style: TextStyle(
                                              fontSize: 10,
                                              fontFamily: 'UTM',
                                              fontWeight: FontWeight.w700,
                                              color: Colors.black,
                                              decoration: TextDecoration.lineThrough
                                          )
                                      )
                                    ],
                                  ),
                                ),
                                Row(
                                    children: [
                                      const Expanded(child: Text("ÁO SƠ MI FAS32791", style: TextStyle(fontSize: 17, fontFamily: 'UTM', fontWeight: FontWeight.w400),)),
                                      const Padding(padding: EdgeInsets.only(right: 15),child: Text("130.000 đ",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w700))),
                                      Container(
                                          width: 35,
                                          height: 17,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(3),
                                              border: Border.all(color: const Color.fromRGBO( 51, 51, 51,0.5))
                                          ),
                                          child: const Center(
                                            child: Text("-30%",style: TextStyle(fontSize: 10,
                                                fontFamily: 'UTM',
                                                fontWeight: FontWeight.w700,
                                                color: Color.fromRGBO( 51, 51, 51,0.5))
                                            ),
                                          )
                                      )
                                    ]
                                ),
                                Padding(padding: const EdgeInsets.symmetric(vertical: 20),
                                    child:Row(
                                      children: [
                                        BoxSizeProduct( size: 's'),
                                        _makeBoxOutOfSize("m"),
                                        BoxSizeProduct( size: 'l',),
                                        BoxSizeProduct( size: 'xl',),
                                        BoxSizeProduct( size: 'xxl',),
                                      ],
                                    )
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    OutlinedButton(
                                      style: OutlinedButton.styleFrom(
                                          backgroundColor: Colors.white,
                                          side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                                          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                                          minimumSize: const Size(163,45)
                                      ),
                                      onPressed: (){},
                                      child: const Text("THÊM DỎ HÀNG",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
                                    ),
                                    OutlinedButton(
                                      style: OutlinedButton.styleFrom(
                                          backgroundColor: Colors.white,
                                          side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                                          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                                          minimumSize: const Size(163,45)
                                      ),
                                      onPressed: (){},
                                      child: const Text("MUA HÀNG",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      TextButton(onPressed: (){}, child: const Text("Hướng dẫn chọn size", style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: 'UTM',
                                          fontWeight: FontWeight.w400,
                                          color: Color.fromRGBO( 51, 51, 51,0.5),
                                          decoration: TextDecoration.underline)
                                      )
                                      ),
                                      Padding(padding: const EdgeInsets.only(right: 20),
                                        child: TextButton(onPressed: (){}, child: const Text("Tìm tại cửa hàng", style: TextStyle(
                                            fontSize: 15,
                                            fontFamily: 'UTM',
                                            fontWeight: FontWeight.w400,
                                            color: Color.fromRGBO( 51, 51, 51,0.5),
                                            decoration: TextDecoration.underline))
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(padding: const EdgeInsets.only(bottom: 10),
                                    child: IconButton(
                                        onPressed: (){
                                          },
                                        icon: Image.asset('assets/icons/iconXemThem.png',width: 18,height: 18)
                                  )
                                ),
                                Container(

                                  child: Text("MÔ TẢ SẢN PHẨM",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w700,color: Colors.black)),
                                  alignment: Alignment.topLeft,
                                  padding: EdgeInsets.only(bottom: 10),
                                ),
                                Text("Áo thiết kế độc quyền bởi thương hiệu thời trang cao cấp Pantio. Item tưởng quen mà trở nên mới lạ đầy lôi cuốn, uyển chuyển kết hợp cùng họa tiết bắt mắt, mang đến cho nàng sự chỉn chu chuyên nghịệp mà không mất đi nét nữ tính, thời thượng.",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Colors.black)),
                                Container(
                                  child: Text("Màu Khác",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w700,color: Colors.black)),
                                  alignment: Alignment.topLeft,
                                  padding: EdgeInsets.only(top: 40,bottom: 10),
                                ),
                                const SizedBox(
                                  child: ProductList(),
                                  height: 260,
                                ),
                                Container(
                                  child: Text("MẶC CÙNG VỚI",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w700,color: Colors.black)),
                                  alignment: Alignment.topLeft,
                                  padding: EdgeInsets.only(top: 20,bottom: 10),
                                ),
                                const SizedBox(
                                  child: ProductList(),
                                  height: 260,
                                ),
                                Container(
                                  child: Text("SẢN PHẨM LIÊN QUAN",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w700,color: Colors.black)),
                                  alignment: Alignment.topLeft,
                                  padding: EdgeInsets.only(top: 20,bottom: 10),
                                ),
                                 Container(
                                  child: ProductList(),
                                  height: 290,
                                  padding: EdgeInsets.only(bottom: 30),
                                )
                              ],
                            ),
                          ),
                );
              }
          )
        ],
      ),
    );
  }
  Widget _makeBoxOutOfSize(String size){
    return Padding(
        padding: const EdgeInsets.only(right: 5),
        child:Stack(
          children: [
            Container(
              width: 45,
              height: 46,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  border: Border.all(color: const Color.fromRGBO( 51, 51, 51,0.5))
              ),
              child: Center(child: Text(size.toUpperCase(), style: const TextStyle(fontSize: 15,color: Color.fromRGBO( 51, 51, 51,0.5), fontFamily: 'UTM', fontWeight: FontWeight.w400))),
            ),
            Positioned(
                child: Image.asset('assets/icons/Line 18.png', width: 45,height: 46,fit: BoxFit.cover),
            )
          ],
        )
    );
  }

}
