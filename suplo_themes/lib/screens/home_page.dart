
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:suplo_themes/models/category.dart';
import 'package:suplo_themes/screens/collection/collection_page.dart';
import 'package:suplo_themes/widgets/input_forms.dart';
import 'package:suplo_themes/widgets/items_list_categories.dart';
import 'package:suplo_themes/widgets/list_product.dart';
import 'package:suplo_themes/widgets/navigation_bar.dart';

class HomePage extends StatefulWidget  {
  const HomePage({Key? key}) : super(key: key);
  static String routeName= "/HomePage";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late TextEditingController emailController= TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    int _currentIndex =0;
    return  Scaffold(
      appBar:  PreferredSize(
        preferredSize:  const Size.fromHeight(60),
        child: NavigationBar(),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(

              child: Column(
                children: [
                  Image.asset("assets/images/homeImages/image3.png",
                    fit: BoxFit.cover,
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                  ),
                  SizedBox(
                      height: 119,
                      child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children:  [
                              const Text("NEW COLLECTION",style: TextStyle(fontSize: 20, fontFamily: 'UTM',fontWeight: FontWeight.w700)),
                              Padding(padding: const EdgeInsets.symmetric(vertical: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    _makeLine(),
                                    Padding(padding: const EdgeInsets.symmetric(horizontal: 20),
                                      child: Image.asset("assets/images/Group124.png"),
                                    ),
                                    _makeLine(),
                                  ],
                                ),
                              )
                            ],
                          )
                      )
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.pushNamed(context, CollectionPage.routeName);
                    },
                    child: Column(
                      children: [
                        Image.asset("assets/images/homeImages/image1.png",
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                        ),
                        Image.asset("assets/images/homeImages/image2.png",
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                        ),
                      ],
                    ),
                  ),
                  Container(
                      color: const Color.fromRGBO(229,229,229, 0.5),
                      height: 119,
                      child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children:  [
                              const Text("NEW ARRIVAL",style: TextStyle(fontSize: 20, fontFamily: 'UTM',fontWeight: FontWeight.w700)),
                              Padding(padding: const EdgeInsets.symmetric(vertical: 5),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      _makeLine(),
                                      Padding(padding: const EdgeInsets.symmetric(horizontal: 20),
                                        child: Image.asset("assets/images/Group124.png"),
                                      ),
                                      _makeLine(),
                                    ]
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Padding(padding: EdgeInsets.symmetric(horizontal: 40),child: Text("Nữ",style: TextStyle(fontSize: 20, fontFamily: 'UTM',fontWeight: FontWeight.w700)),),
                                  Padding(padding: EdgeInsets.symmetric(horizontal: 40),child: Text("Nam",style: TextStyle(fontSize: 20, fontFamily: 'UTM',fontWeight: FontWeight.w700, color: Color.fromRGBO( 51, 51, 51,0.5))),)
                                ],
                              )
                            ],
                          )
                      )
                  ),
                  Container(
                    color: const Color.fromRGBO(229,229,229, 0.5),
                    child: Column(
                      children: [
                         Container(
                          height: 278,
                          padding: EdgeInsets.only(left: 15),
                          child: ProductList(),
                        ),
                        Padding(padding: const EdgeInsets.only(top:20,bottom: 32),
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                                backgroundColor: Colors.white,
                                side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                                shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                                minimumSize: Size(134,45)
                            ),
                            onPressed: (){},
                            child: const Text("XEM THÊM",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 818,
                    child: Column(
                      children: [
                        Center(
                            child: Padding(
                                padding: const EdgeInsets.only(top: 30,bottom: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children:  [
                                    const Text("FEATURED CATEGORIES",style: TextStyle(fontSize: 20, fontFamily: 'UTM',fontWeight: FontWeight.w700)),
                                    Padding(padding: const EdgeInsets.symmetric(vertical: 5),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          _makeLine(),
                                          Padding(padding: const EdgeInsets.symmetric(horizontal: 20),
                                            child: Image.asset("assets/images/Group124.png"),
                                          ),
                                          _makeLine(),
                                        ],
                                      ),
                                    )
                                  ],
                                )
                            )
                        ),
                        _makeListCategories()
                      ],
                    ),
                  ),
                  Image.asset("assets/images/homeImages/collection1.png",
                    fit: BoxFit.cover,
                    width: MediaQuery.of(context).size.width,
                  ),
                  Container(
                      color: const Color.fromRGBO(229,229,229, 0.5),
                      child: Padding(padding: const EdgeInsets.only(top:28),
                        child: Column(
                          children:  [
                            const Padding(padding: EdgeInsets.symmetric(horizontal: 16),child: Text('/"HOA VIÊN"/ - BST ÁO DÀI ĐÍNH KẾT HOA THỦ CÔNG 2020',style: TextStyle(fontSize: 24, fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                            Padding(padding: const EdgeInsets.symmetric(vertical: 5),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    _makeLine(),
                                    Padding(padding: const EdgeInsets.symmetric(horizontal: 20),
                                      child: Image.asset("assets/images/Group124.png"),
                                    ),
                                    _makeLine(),
                                  ]
                              ),
                            ),
                            const Padding(padding: EdgeInsets.symmetric(horizontal: 16),
                                child: Text('Lấy cảm hứng từ nét đẹp mềm mại, thanh khiết, quyến rũ của của các loài hoa, BST áo dài Thu 2020 “HOA VIÊN” với ý tưởng phác họa hình ảnh 1 vườn hoa đầy hương sắc được Pantio chính thức ra mắt trong tháng 10 nhằm tri ân tới đông đảo khách hàng của mình. Với sự vận dụng màu sắc linh hoạt và sáng tạo theo bảng màu xu hướng Thu 2020 như Vàng – Đen, Đỏ - Đen, Xanh lá, Hồng đất… ',
                                    style: TextStyle(fontSize: 15, fontFamily: 'UTM',fontWeight: FontWeight.w400))),
                            Padding(padding: const EdgeInsets.only(top:20,bottom: 32),
                                child: OutlinedButton(
                                  style: OutlinedButton.styleFrom(
                                      backgroundColor: Colors.white,
                                      side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                                      minimumSize: const Size(134,45)
                                  ),
                                  onPressed: (){},
                                  child: const Text("XEM THÊM",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
                                ),
                            ),
                          ],
                        ),
                      )
                  ),
                  Container(

                    child: Column(
                      children: [
                        Padding(padding: const EdgeInsets.only(top:30,bottom: 20),child: Text('đăng ký nhận tin từ pantio'.toUpperCase(),style: TextStyle(fontSize: 20, fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        Padding(padding: const EdgeInsets.only(left: 16,right: 16,bottom: 20),child: InputForm(controller: emailController,hinText: "Nhập email của bạn")),
                        Padding(padding: const EdgeInsets.only(bottom: 30),
                            child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                  backgroundColor: Colors.white,
                                  side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                                  shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                                  minimumSize: Size(123,45)
                              ),
                              onPressed: (){},
                              child: const Text("ĐĂNG KÝ",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
                            ),
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 17),child: Text('about us'.toUpperCase(),style: TextStyle(fontSize: 20,fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        const Padding(padding: EdgeInsets.symmetric(horizontal: 16),
                            child: Text(
                                'Luôn đón đầu các xu hướng thời trang trên thế giới, Pantio hướng tới phong cách thời trang tối giản, trẻ trung và hiện đại. Cùng Pantio tự tin định hình phong cách thời trang của chính mình, để đón nhận thành công trong cuộc sống. “Pantio - Biểu tượng tươi trẻ và năng lượng tích cực”.',
                                style: TextStyle(fontSize: 15, fontFamily: 'UTM',fontWeight: FontWeight.w400))),
                        Padding(padding: EdgeInsets.only(bottom: 23,top:30),child: Text('hỗ trợ mua hàng'.toUpperCase(),style: TextStyle(fontSize: 18,fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        Padding(padding: EdgeInsets.only(bottom: 18),child: Text('Chính sách mua hàng online'.toUpperCase(),style: TextStyle(fontSize: 18,fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        _makeDivider(),
                        Padding(padding: EdgeInsets.only(bottom: 18,top:20),child: Text('Chính sách đổi trả'.toUpperCase(),style: TextStyle(fontSize: 18,fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        _makeDivider(),
                        Padding(padding: EdgeInsets.only(bottom: 18,top:20),child: Text('Chính sách bảo hành'.toUpperCase(),style: TextStyle(fontSize: 18,fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        _makeDivider(),
                        Padding(padding: EdgeInsets.only(bottom: 18,top:20),child: Text('Chính sách thẻ thành viên - thẻ VIP'.toUpperCase(),style: TextStyle(fontSize: 18,fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        _makeDivider(),
                        Padding(padding: EdgeInsets.only(bottom: 43),child: Text('Điều khoản dịch vụ'.toUpperCase(),style: TextStyle(fontSize: 20,fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            _makeImageButton("assets/images/homeImages/f.png"),
                            _makeImageButton("assets/images/homeImages/in.png"),
                            _makeImageButton("assets/images/homeImages/you.png")
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 23,top:35),child: Text('thông tin liên hệ'.toUpperCase(),style: TextStyle(fontSize: 20,fontFamily: 'UTM',fontWeight: FontWeight.w700))),
                        Row(
                          children: [
                            Padding(padding: EdgeInsets.only(left: 17.11,right: 23),child: Icon(Icons.phone_outlined) ),
                            Text('CSKH 1900 988 903'.toUpperCase(),style: TextStyle(fontSize: 15,fontFamily: 'UTM',fontWeight: FontWeight.w400)),
                          ],
                        ),
                        Row(
                          children: [
                            Padding(padding: EdgeInsets.only(left: 17.11,right: 23),child: Icon(Icons.phone_outlined) ),
                            Flexible(child: Text('MUA HÀNG 0243 5725 999 – 096 301 4949 – 0962 877 733'.toUpperCase(),style: TextStyle(fontSize: 15,fontFamily: 'UTM',fontWeight: FontWeight.w400)))
                          ],
                        ),
                        Row(
                          children: [
                            Padding(padding: EdgeInsets.only(left: 17.11,right: 23),child: Icon(Icons.language_outlined) ),
                            Text('cskh@pantio.vn'.toUpperCase(),style: TextStyle(fontSize: 15,fontFamily: 'UTM',fontWeight: FontWeight.w400)),
                          ],
                        ),
                        Padding(padding: const EdgeInsets.only(top:42,bottom: 35),
                            child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                  backgroundColor: Colors.white,
                                  side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                                  shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                                  minimumSize: Size(134,45)
                              ),
                              onPressed: (){},
                              child:  Text('hệ thống 36 showroom'.toUpperCase(),style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
                            ),
                        ),
                        _makeDivider(),
                        Padding(padding: const EdgeInsets.only(top:25,bottom: 20),
                          child: Image.asset("assets/images/BCT.png"),
                        ),
                        const Padding(padding: EdgeInsets.only(bottom: 30,right: 16,left: 16),
                            child: Center(
                              child:  Text('CÔNG TY THỜI TRANG H&A Địa chỉ văn phòng: Số 9A Tòa nhà Hải Ngân ( Đối diện sân bóng Bộ Công An), Ngõ 396 Nguyễn Xiển, Thanh Trì, Hà Nội.',
                                  style: TextStyle(fontSize: 15, fontFamily: 'UTM',fontWeight: FontWeight.w400)),
                            )
                        ),
                      ],
                    ),

                  )
                ],
              )
          ),
          Positioned(bottom: 40,right: 20, child: IconButton(
            icon: Image.asset('assets/images/Frame.png'),
            iconSize: 50,
            onPressed: () {},
            )
          ),

        ],
      ),
        bottomNavigationBar: BottomNavigationBar(

          unselectedItemColor: const Color.fromRGBO(51, 51, 51, 1),
          selectedItemColor: const Color.fromRGBO(125, 0, 0, 1),
          // unselectedLabelStyle: const TextStyle(color: Color.fromRGBO(51, 51, 51, 1)),
          showUnselectedLabels: true,
          currentIndex: _currentIndex,
          items: <BottomNavigationBarItem>[
             const BottomNavigationBarItem(icon: Icon(Icons.home_outlined), label: "Trang chủ"),
             const BottomNavigationBarItem(icon: Icon(Icons.local_offer_outlined ), label: "Sản phẩm"),
             const BottomNavigationBarItem(icon: Icon(Icons.fmd_good_outlined ), label: "Cửa hàng",),
             BottomNavigationBarItem(icon: Stack(
              children: [
                const Icon(Icons.notifications_none_outlined ),
                Positioned(
                  top: 0,
                  right: 0 ,
                  child:  Container(
                      height: 15,
                      width: 15,
                      decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(13),
                      color: Colors.red
                      ),
                      child: const Center(child: Text("10",style: TextStyle(fontSize: 10,color: Colors.white))),
                  ),
                )
              ],
            ), label: "Thông báo" ),
             const BottomNavigationBarItem(icon: Icon(Icons.account_circle_outlined),label: "Tài Khoản" )

          ],
          onTap: (index){
          setState(() {
            _currentIndex= index;
            print(_currentIndex);
          });
        },
      ),
    );
  }
  Widget _makeDivider(){
    return const Divider(
      height: 20,
      thickness: 2,
      indent: 0,
      endIndent: 0,
    );
  }
  Widget _makeImageButton(String imageUrl){
    return IconButton(
      icon: Image.asset(imageUrl),
      iconSize: 50,
      onPressed: () {},
    );
  }
  Widget  _makeLine(){
    return  Container(
      height: 1,
      width: 59,
      color: const Color.fromARGB(255,51, 51, 51),
    );
  }

  Widget _makeListCategories() {

    return GridView(
      shrinkWrap: true,
      primary: false,
      padding: const EdgeInsets.all(12),
      children: FAKE_DATA_CATEGORIES.map((e) => ItemListCategory(category: e)).toList(),
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 300, //width / height
          crossAxisSpacing: 15,
          childAspectRatio: 164/220,
          mainAxisSpacing: 15,
          mainAxisExtent: 220
      ),
    );
  }
}

