import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:suplo_themes/models/product.dart';
import 'package:suplo_themes/widgets/box_size_product.dart';
import 'package:suplo_themes/widgets/cart_icon.dart';
import 'package:suplo_themes/widgets/items_gridview_product.dart';
import 'package:suplo_themes/widgets/list_colors.dart';
final scaffoldState = GlobalKey<ScaffoldState>();
class ListProductPage extends StatefulWidget {
  static String routeName ="/listProductPage";
  const ListProductPage({Key? key}) : super(key: key);

  @override
  State<ListProductPage> createState() => _ListProductPageState();
}

class _ListProductPageState extends State<ListProductPage> {
  List<String> tabBarItem = ["ÁO SƠ MI", "ÁO THUN ", "ÁO VEST", "ÁO LEN"];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar:  AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            titleSpacing: 0.0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  icon: const Icon(Icons.arrow_back_sharp, color: Colors.black,),
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    IconButton(
                      icon: const Icon(Icons.menu,color: Colors.black,),
                      onPressed:(){},
                    ),
                    Positioned(
                      top: 12.0,
                      right: 10.0,
                      width: 10.0,
                      height: 10.0,
                      child: Container(
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: Center(child: Image.asset("assets/images/logo.png")),
                )
              ],
            ),
            automaticallyImplyLeading: false,
            centerTitle: true,

            actions: [
              IconButton(
                  onPressed: (){},
                  icon: const Icon(Icons.search ,color: Colors.black,size: 30)),
              const Padding(
                padding: EdgeInsets.only(right: 16),
                child: CartIcon(),
              )
            ],
            bottom:TabBar(
              indicatorColor: Colors.transparent,
              unselectedLabelColor: const Color.fromRGBO( 51, 51, 51,0.5),
              labelColor: Colors.black,
              isScrollable: true,

              tabs: [
                _makeItemTabBar("ÁO SƠ MI"),
                _makeItemTabBar("ÁO THUN"),
                _makeItemTabBar("ÁO VEST"),
                _makeItemTabBar("ÁO LEN"),
              ],
            )
        ),
        body: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 22),
            child:  Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:  [
                      TextButton(onPressed: (){_softBottomSheet(context);}, child: const Text("Sắp xếp", style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "UTM", fontSize: 15, color: Colors.black),),
                      ),
                      TextButton(onPressed: (){_filterBottomSheet(context);}, child: const Text("Bộ lọc", style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "UTM", fontSize: 15,color: Colors.black)),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8,),
                  Expanded(child: _makeListCategories())
                ],
              ),
          ),
        )
      ),
    );
  }
  Widget _makeItemTabBar(String lable){
    return Container(
        padding: const EdgeInsets.only(right: 30) ,
        height: 50,
        child: Center(child: Text(lable, style: const TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w700,),),)
    );
  }
  Widget _makeListCategories() {
    return GridView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: FAKE_DATA_PRODUCT.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 165/270,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10
        ),
      itemBuilder: (BuildContext context, int index) { 
        return ItemGridViewProduct(index: index);
      },
    );
  }
  _softBottomSheet(context){
    showModalBottomSheet(context: context, builder: (BuildContext c){
      return Container(
        color: const Color(0xFF737373) ,
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
            color: Colors.white,
          ),
          height: 120 ,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextButton(onPressed: (){}, child: const Text("Giá từ thấp đến cao", style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "UTM", fontSize: 15, color: Colors.black),),
              ),
              const Divider(
                height: 20,
                thickness: 1,
                indent: 0,
                endIndent: 0,
              ),
              TextButton(onPressed: (){}, child: const Text("Giá từ cao đến thấp", style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "UTM", fontSize: 15,color: Colors.black)),
              ),
            ],
          ),
        ),
      );
    });
  }
  _filterBottomSheet(context){
    showModalBottomSheet(context: context, builder: (BuildContext c){
      return Container(
        color: const Color(0xFF737373) ,
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
            color: Colors.white,
          ),
          height:320 ,
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
          child: Column(

            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: (){},
                    child: Row(
                      children: [
                        Image.asset("assets/icons/filt2.png"),
                        const SizedBox(width: 6),
                        const Text("Bỏ Lọc", style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "UTM", fontSize: 13,color: Colors.black))
                      ],
                    ),
                  ),

                  const Padding(padding: EdgeInsets.only(right: 15),child: Text("BỘ LỌC", style: TextStyle(fontWeight: FontWeight.w700, fontFamily: "UTM", fontSize: 15,color: Colors.black))),
                  IconButton(onPressed: (){
                    Navigator.pop(context);
                  },
                      padding: const EdgeInsets.only(left: 20),
                      icon: Image.asset('assets/icons/x.png')
                  )
                ],
              ),
              const SizedBox(height: 15,),
              const Text("Chọn màu", style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "UTM", fontSize: 15,color: Colors.black)),
              ListColors(),
              const Text("Chọn size", style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "UTM", fontSize: 15,color: Colors.black)),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  BoxSizeProduct( size: 's'),
                  BoxSizeProduct( size: 'm'),
                  BoxSizeProduct( size: 'l'),
                  BoxSizeProduct( size: 'xl'),
                  BoxSizeProduct( size: 'xxl'),
                ],
              ),
              const SizedBox(height: 20),
              OutlinedButton(
                style: OutlinedButton.styleFrom(
                    backgroundColor: Colors.white,
                    side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 0.5)),
                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                    minimumSize: const Size(311,53),
                ),
                onPressed: (){},
                child: const Text("LỌC",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
              ),

            ],
          ),
        ),
      );
    });
  }
}

