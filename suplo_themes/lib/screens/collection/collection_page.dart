import 'package:flutter/material.dart';
import 'package:suplo_themes/screens/collection/detail_collection_page.dart';
import 'package:suplo_themes/widgets/cart_icon.dart';

class CollectionPage extends StatelessWidget {
  const CollectionPage({Key? key}) : super(key: key);
  static String routeName ="/collectionPage";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          titleSpacing: 0.0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconButton(
                icon: const Icon(Icons.arrow_back_sharp, color: Colors.black,),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
              Stack(
                alignment: Alignment.center,
                children: [
                  IconButton(
                    icon: Icon(Icons.menu,color: Colors.black,),
                    onPressed:(){},
                  ),
                  Positioned(
                    top: 12.0,
                    right: 10.0,
                    width: 10.0,
                    height: 10.0,
                    child: Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                    ),
                  )
                ],
              ),
              Expanded(
                child: Center(child: Image.asset("assets/images/logo.png")),
              )
            ],
          ),
          automaticallyImplyLeading: false,
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: (){},
                icon: const Icon(Icons.search ,color: Colors.black,size: 30)),
            const Padding(
              padding: EdgeInsets.only(right: 16),
              child: CartIcon(),
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                InkWell(
                  onTap: (){
                    Navigator.pushNamed(context, DetailCollectionPage.routeName);
                  },
                  child:  Image.asset("assets/images/collection/cllection1-pic1.png",
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width,
                  ),
                ),
                InkWell(
                  onTap: (){},
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 16),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Image.asset("assets/images/collection/collection2.png",
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width,
                    ),
                  ),
                )
              ],
            ) ,
          ),
          Positioned(bottom: 40,right: 20, child: IconButton(
            icon: Image.asset('assets/images/Frame.png'),
            iconSize: 50,
            onPressed: () {},
            )
          ),
        ],
      ),
    );
  }
}
