import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:suplo_themes/widgets/cart_icon.dart';
import 'package:suplo_themes/widgets/list_product.dart';

class DetailCollectionPage extends StatelessWidget {
  const DetailCollectionPage({Key? key}) : super(key: key);
  static String routeName= "/detailCollectionPage";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          titleSpacing: 0.0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconButton(
                icon: const Icon(Icons.arrow_back_sharp, color: Colors.black,),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
              Stack(
                alignment: Alignment.center,
                children: [
                  IconButton(
                    icon: const Icon(Icons.menu,color: Colors.black,),
                    onPressed:(){},
                  ),
                  Positioned(
                    top: 12.0,
                    right: 10.0,
                    width: 10.0,
                    height: 10.0,
                    child: Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                    ),
                  )
                ],
              ),
              Expanded(
                child: Center(child: Image.asset("assets/images/logo.png")),
              )
            ],
          ),
          automaticallyImplyLeading: false,
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: (){},
                icon: const Icon(Icons.search ,color: Colors.black,size: 30)),
            const Padding(
              padding: EdgeInsets.only(right: 16),
              child: CartIcon(),
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                InkWell(
                  onTap: (){},
                  child:  Image.asset("assets/images/collection/cllection1-pic1.png",
                    fit: BoxFit.cover,
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
                InkWell(
                  onTap: (){},
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 16),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Column(
                      children: [
                        Image.asset("assets/images/collection/cllection1-pic2.png",
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                        ),
                        Image.asset("assets/images/collection/cllection1-pic3.png",
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                        ),
                        Image.asset("assets/images/collection/cllection1-pic4.png",
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top:55,left: 16),
                  child: Column(
                    children: [
                      Container(
                        child: const Text("SẢN PHẨM CÓ TRONG ALBUM",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w700,color: Colors.black)),
                        alignment: Alignment.topLeft,
                      ),
                      Container(
                        child: const ProductList(),
                        height: 300,
                        padding: const EdgeInsets.only(top:10,bottom: 30),
                      )
                    ],
                  ),
                )

              ],
            ),
          ),
          Positioned(bottom: 40,right: 20, child: IconButton(
            icon: Image.asset('assets/images/Frame.png'),
            iconSize: 50,
            onPressed: () {},
          )
          ),
        ],
      ),
    );
  }
}
