import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:suplo_themes/screens/home_page.dart';
import 'package:suplo_themes/screens/login/register_page.dart';
import 'package:suplo_themes/widgets/input_forms.dart';
import 'package:suplo_themes/widgets/password_input_form.dart';

enum MobileVerificationState{
 SHOW_MOBILE_FORM_STATE,
  SHOW_OTP_FORM_STATE,

}
class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}
class _LoginPageState extends State<LoginPage> {
  late TextEditingController phoneNumberController;
  TextEditingController passwordController= TextEditingController();
  TextEditingController fistOtpController= TextEditingController();
  TextEditingController secondOtpController= TextEditingController();
  TextEditingController thirdOtpController= TextEditingController();
  TextEditingController fourthOtpController= TextEditingController();
  TextEditingController fifthOtpController= TextEditingController();
  TextEditingController lastOtpController= TextEditingController();
  late String otp;
  MobileVerificationState currentState =MobileVerificationState.SHOW_MOBILE_FORM_STATE;
  final FirebaseAuth _auth =FirebaseAuth.instance;
  final GlobalKey<ScaffoldState> _globalKey =GlobalKey();
  late String verificationId;
  bool showLoading= false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    phoneNumberController= TextEditingController();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    passwordController.dispose();
    //phoneNumberController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child:  showLoading? const Center(child: CircularProgressIndicator(),) :currentState== MobileVerificationState.SHOW_MOBILE_FORM_STATE?
            getLoginPage():GetOtpPage(),
      )
    );
  }
  Widget getLoginPage(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20 ),
      child: Form(
        child: ListView(
          children: [
            Center(
                child: Image.asset("assets/images/login/LogoLogin.png",)
            ),
            const SizedBox(height: 20,),
            Row(
              children: const [
                Text("Số điện thoại"),
                Text("*", style: TextStyle(color: Colors.red),)
              ],
            ),
            const SizedBox(height: 13),
            InputForm(hinText: "Nhập số điện thoại của bạn", controller: phoneNumberController),
            const SizedBox(height: 15),
            Row(
              children: const [
                Text("Mật khẩu"),
                Text("*", style: TextStyle(color: Colors.red),)
              ],
            ),
            const SizedBox(height: 13),
            PasswordInputForm(controller: passwordController,hinText: "Nhập mật khẩu của bạn"),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(onPressed: (){}, child: const Text("Quên mật khẩu ?", style: TextStyle(
                  fontSize: 13,
                  fontFamily: 'UTM',
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO( 51, 51, 51,0.5),))
                ),
              ],
            ),
            OutlinedButton(
              style: OutlinedButton.styleFrom(
                  backgroundColor: Colors.white,
                  side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                  shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                  minimumSize: const Size(343,53)
              ),
              onPressed: () async{
                print(phoneNumberController.text);
                setState(() {
                  showLoading= true;
                });
               await  _auth.verifyPhoneNumber(
                    phoneNumber: phoneNumberController.text,
                    verificationCompleted: (phoneAuthCredential) async {
                      setState(() {
                        showLoading= false;
                      });
                    },
                    verificationFailed: (verificationFailed) async {
                      setState(() {
                        showLoading= false;
                      });
                        ScaffoldMessenger.of(context).showSnackBar( SnackBar(content: Text(verificationFailed.message.toString())));
                    },
                    codeSent: (verificationId, reSendingtoken) async{
                      setState(() {
                        showLoading = false;
                        currentState= MobileVerificationState.SHOW_OTP_FORM_STATE;
                        this.verificationId =verificationId;
                      });
                    },
                    codeAutoRetrievalTimeout: (verificationId)async{}
                );
                // setState(() {
                //   showLoading= false;
                // });
              },
              child: const Text("ĐĂNG NHẬP",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
            ),
            const SizedBox(height: 10),
            const Center( child: Text("Hoặc kết nối với",style: TextStyle(
              fontSize: 13,
              fontFamily: 'UTM',
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO( 51, 51, 51,0.5)))),
            const SizedBox(height: 10),
            OutlinedButton(
                style: OutlinedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(71, 89, 147, 1),
                    side: const BorderSide(color: Color.fromRGBO(71, 89, 147, 1)),
                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                    minimumSize: const Size(343,53)
                ),
                onPressed: (){
                  Navigator.pushNamed(context, HomePage.routeName);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/images/login/facebook.png",),
                    const SizedBox(width: 15,),
                    Text("đăng nhập bằng facebook".toUpperCase(),style: const TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Colors.white)),
                  ],
                )
            ),
            const SizedBox(height: 10,),
            OutlinedButton(
                style: OutlinedButton.styleFrom(
                    backgroundColor:  Colors.white,
                    side: const BorderSide(color: Color.fromRGBO(71, 89, 147, 1)),
                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                    minimumSize: const Size(343,53)
                ),
                onPressed: (){
                  Navigator.pushNamed(context, HomePage.routeName);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/images/login/google.png",),
                    const SizedBox(width: 15,),
                    Text("đăng nhập bằng google".toUpperCase(),style: const TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Colors.black)),
                  ],
                )
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children:  [
                const Text("Bạn chưa có tài khoản ?",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: 'UTM',
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO( 51, 51, 51,0.5))
                ),
                TextButton(
                    onPressed: (){},
                    child: const Text("Đăng kí", style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'UTM',
                      fontWeight: FontWeight.w700,
                      color: Colors.black))
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
  Widget GetOtpPage(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20 ),
      child: Form(
        child: ListView(
          children: [
            Center(
                child: Image.asset("assets/images/login/LogoLogin.png",)),
                const SizedBox(height: 37,),
                const Center(child: Text("XÁC NHẬN MÃ OTP",style: TextStyle(
                    fontSize: 17,
                    fontFamily: 'UTM',
                    fontWeight: FontWeight.w700,
                    color: Colors.black))),
                const SizedBox(height: 10,),
                const Center(child: Text("Mã OTP sẽ được gửi vào số điện thoại của bạn",style: TextStyle(
                    fontSize: 13,
                    fontFamily: 'UTM',
                    fontWeight: FontWeight.w700,
                    color: Color.fromRGBO(51, 51, 51, 0.5)))),
                const SizedBox(height: 40,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: [
                    _textFieldOTP(first: true, last: false,textController: fistOtpController),
                    _textFieldOTP(first: false, last: false, textController: secondOtpController),
                    _textFieldOTP(first: false, last: false, textController: thirdOtpController),
                    _textFieldOTP(first: false, last: false,textController:  fourthOtpController),
                    _textFieldOTP(first: false, last: false, textController: fifthOtpController),
                    _textFieldOTP(first: false, last: true,textController:  lastOtpController),
                  ],
                ),
              const SizedBox(height: 60),
              OutlinedButton(
                style: OutlinedButton.styleFrom(
                    backgroundColor: Colors.white,
                    side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                    minimumSize: const Size(343,53)
                ),
                onPressed: () async{
                  otp=fistOtpController.text+secondOtpController.text+thirdOtpController.text+fourthOtpController.text+fifthOtpController.text+ lastOtpController.text;
                    PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(verificationId: verificationId, smsCode: otp);
                    signInPhoneAuthCredential( phoneAuthCredential);
                  },
                child: const Text("XÁC NHẬN",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
            ),
            ],
          ),
        )
      );
  }
  Widget _textFieldOTP({required bool first, last,required TextEditingController textController}) {
    return Container(
      //height: 60,
      margin: const EdgeInsets.symmetric(horizontal: 5),
      width: 50,
      child: AspectRatio(
        aspectRatio: 1.0,
        child: TextField(
          controller: textController,
          //autofocus: true,

          onChanged: (value) {
            if (value.length == 1 && last == false) {
              FocusScope.of(context).nextFocus();
            }
            if (value.isEmpty && first == false) {
              FocusScope.of(context).previousFocus();
            }
          },
          showCursor: false,
          readOnly: false,
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          keyboardType: TextInputType.number,
          maxLength: 1,
          decoration: const InputDecoration(
            counter: Offstage(),
          ),
        ),
      ),
    );
  }
  void signInPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) async{
    setState(() {
      showLoading= true;
    });
   try {
     final authCredential= await _auth.signInWithCredential(phoneAuthCredential);
     setState(() {
       showLoading= false;
     });
     if(authCredential.user != null){
       Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomePage()));
     }
   } on FirebaseAuthException catch (e) {
     showLoading= false;
     ScaffoldMessenger.of(context).showSnackBar( const SnackBar(content: Text("Invalid OTP")));
   }
    setState(() {
      //showLoading= false;
    });
  }
}

