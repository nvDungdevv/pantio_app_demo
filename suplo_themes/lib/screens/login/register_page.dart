import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);
  static String routeName ="/RegisterPage";

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController _numController=TextEditingController();
  final TextEditingController _passController=TextEditingController();
  final TextEditingController _repassController= TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: Colors.black
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text("TÀI KHOẢN", style: TextStyle(color: Colors.black),),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 16),
        child: Form(
            child: ListView(
              children: [
                TextFormField(
                  controller: _numController,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.email),
                    labelText: 'Số điện thoại*',
                  ),
                  keyboardType: TextInputType.number,
                  autocorrect: false,
                  autovalidateMode: AutovalidateMode.always,
                ),
                TextFormField(
                  controller: _passController,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.lock),
                    labelText: 'Password',
                  ),
                  obscureText: true,
                  autocorrect: false,
                  autovalidateMode: AutovalidateMode.always,
                ),
                TextFormField(
                  controller: _repassController,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.lock),
                    labelText: 'RePassword',
                  ),
                  obscureText: true,
                  autocorrect: false,
                  autovalidateMode: AutovalidateMode.always,
                ),
                const SizedBox(height: 20,),
                OutlinedButton(
                  style: OutlinedButton.styleFrom(
                      backgroundColor: Colors.white,
                      side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                      minimumSize: const Size(163,45)
                  ),
                  onPressed: (){
                    _onRegister();

                  },
                  child: const Text("ĐĂNG KÍ",style: TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1))),
                ),


              ],
            ),
        ),
      ),
    );
  }
  void _onRegister(){
    print(_numController.text);
    print(_passController.text);
    print(_repassController.text);

  }
}
