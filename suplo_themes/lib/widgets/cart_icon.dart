
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:suplo_themes/themes/c_icons.dart';

class CartIcon extends StatefulWidget {
  const CartIcon({Key? key}) : super(key: key);

  @override
  _CartIconState createState() => _CartIconState();
}

class _CartIconState extends State<CartIcon> {
  @override
  Widget build(BuildContext context) {
    return Badge(
      position: BadgePosition.topEnd(top: 6, end: 6),
      padding: const EdgeInsets.all(3),
      animationDuration: const Duration(milliseconds: 300),
      animationType: BadgeAnimationType.slide,
      badgeContent: const Center(child: Text("10",style: TextStyle(fontSize: 10,color: Colors.white))),
      child: IconButton(
          icon: Icon(Icons.shopping_cart,color: Colors.black,size: 30),
          onPressed: () {}),
    );
  }
}
