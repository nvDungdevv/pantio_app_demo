
import 'package:flutter/material.dart';

class InputForm extends StatefulWidget {
  String hinText;
  TextEditingController controller;
  InputForm({required this.hinText,Key? key, required this.controller}) : super(key: key);


  @override
  _InputFormState createState() => _InputFormState();
}

class _InputFormState extends State<InputForm> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.controller = TextEditingController();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    widget.controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: false,
      controller: widget.controller,
      //keyboardType: TextInputType.number,
        autofocus: false,
      decoration: InputDecoration(
        border:  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        hintStyle: const TextStyle(fontSize: 13, fontFamily: 'UTM',fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 0.5)),
        hintText: widget.hinText,
      )

    );
  }
}
