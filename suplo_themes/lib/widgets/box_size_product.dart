import 'package:flutter/material.dart';

class BoxSizeProduct extends StatefulWidget {
  String size;
  BoxSizeProduct({Key? key,required this.size}) : super(key: key);
  @override
  _BoxSizeProductState createState() => _BoxSizeProductState();
}
class _BoxSizeProductState extends State<BoxSizeProduct> {
  bool pickColor=false;
  @override
  Widget build(BuildContext context) {

    return Padding(
        padding: const EdgeInsets.only(right: 5),
        child:InkWell(
          onTap: (){
           setState(() {
             pickColor=!pickColor;
           });
          },
          child: Container(
            width: 45,
            height: 46,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3),
                border: Border.all(color: pickColor?  Colors.black : const Color.fromRGBO( 51, 51, 51,0.5))
            ),
            child: Center(child: Text(widget.size.toUpperCase(), style: TextStyle(fontSize: 15,color: pickColor?  Colors.black : const Color.fromRGBO( 51, 51, 51,0.5), fontFamily: 'UTM', fontWeight: FontWeight.w400))),
          ),
        )
    );
  }
}
