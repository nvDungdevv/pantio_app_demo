
import 'package:flutter/material.dart';
import 'package:suplo_themes/models/category.dart';
import 'package:suplo_themes/screens/product/list_product_page.dart';

class ItemListCategory extends StatelessWidget {
  Category category;
  ItemListCategory({required this.category,Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, ListProductPage.routeName);
      },
      child:  Container(
        height: 220,
        width: 164,
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0))
        ),
        child: Stack(
          children: [
            Image.asset(category.imageUrl!,fit: BoxFit.cover,height: 220, width: 164,),
            Positioned(
                top:180,
                child: Container(
                  width: 164,
                  height: 40,
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(90, 94, 106, 1.0).withOpacity(0.4),
                      borderRadius: const BorderRadius.only(bottomRight: Radius.circular(8.0),bottomLeft: Radius.circular(8.0))
                  ),
                  child: Center(child: Text(category.nameOfCategory!,style:const TextStyle(fontSize: 15, fontFamily: 'UTM',fontWeight: FontWeight.w700,color: Color.fromRGBO(255, 255, 255, 1)))),
                )
            )
          ],
        ),
      ),
    );
  }
}
