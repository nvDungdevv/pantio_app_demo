
import 'package:flutter/material.dart';

class PasswordInputForm extends StatefulWidget {
  String hinText;
  TextEditingController controller;
  PasswordInputForm({required this.hinText,Key? key, required this.controller}) : super(key: key);


  @override
  _PasswordInputFormState createState() => _PasswordInputFormState();
}

class _PasswordInputFormState extends State<PasswordInputForm> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.controller = TextEditingController();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    //widget.controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return TextField(
        obscureText: true,
        controller: widget.controller,
        decoration: InputDecoration(
          suffixIcon: Image.asset('assets/icons/eye.png'),
          border:  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
          hintStyle: const TextStyle(fontSize: 13, fontFamily: 'UTM',fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 0.5)),
          hintText: widget.hinText,
        )
    );
  }
}
