
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:suplo_themes/models/product.dart';
import 'package:suplo_themes/screens/product_detail_page.dart';

class ItemGridViewProduct extends StatefulWidget {
  int index;
  ItemGridViewProduct({Key? key, required this.index}) : super(key: key);


  @override
  State<ItemGridViewProduct> createState() => _ItemGridViewProductState();
}

class _ItemGridViewProductState extends State<ItemGridViewProduct> {
  bool favorite= false;
  @override
  Widget build(BuildContext context) {
    var f = NumberFormat('###.0#', 'en_US');
    return InkWell(
      onTap: (){
        Navigator.pushNamed(context, ProductDetailPage.routeName);
      },
      child: Container(

        margin: const EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: const Color.fromRGBO( 161, 158, 158, 0.30196078431372547) ,width: 1),
          borderRadius: const BorderRadius.all(Radius.circular(8.0)),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Image.asset(FAKE_DATA_PRODUCT[widget.index].imageUrl!,fit: BoxFit.cover,
                  height: 241,
                  width: MediaQuery.of(context).size.width,),
                Positioned(
                    top:0,
                    right: 0,
                    child: IconButton(
                      icon: Icon(Icons.favorite_border,color: favorite?  Colors.red: const Color.fromRGBO( 51, 51, 51,0.5)),
                      onPressed: () {
                        setState(() {
                          favorite=!favorite;
                        });
                      },
                    )
                ),
                Positioned(
                  left: 2,
                    bottom: 22,
                    child: Column(
                      children: [
                        Container(
                          width: 35,
                          height: 15,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            color: Color.fromRGBO( 51, 51, 51,0.5)
                          ),
                          child: const Center(child: Text("Hot",style: TextStyle(color: Colors.white,fontSize: 8,fontWeight: FontWeight.w400),),),
                        ),
                        SizedBox(height: 4,),
                        Container(
                          width: 35,
                          height: 15,
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(30)),
                              color: Color.fromRGBO( 51, 51, 51,0.5)
                          ),
                          child: const Center(child: Text("Sale",style: TextStyle(color: Colors.white,fontSize: 8,fontWeight: FontWeight.w400),),),
                        )
                      ],
                    )
                )
              ],
            ),

            Column(
                children: [
                  Padding(padding: const EdgeInsets.only(top: 10,left: 8,right: 5),
                      child: Text(
                        FAKE_DATA_PRODUCT[widget.index].productName!,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontSize: 13, fontFamily: 'UTM',fontWeight: FontWeight.w400),
                      )
                  ),
                  Row(
                    children: [
                      Padding(padding:  const EdgeInsets.only(left: 8, right: 4),
                          child:Text(NumberFormat.currency(locale: 'eu', symbol: 'đ',decimalDigits: 0).format(FAKE_DATA_PRODUCT[widget.index].salePrice),style: const TextStyle(fontSize: 13, fontFamily: 'UTM', fontWeight: FontWeight.w700))
                      ),
                      Text(NumberFormat.currency(locale: 'eu', symbol: 'đ',decimalDigits: 0).format(FAKE_DATA_PRODUCT[widget.index].price),
                          style: const TextStyle(
                              fontSize: 10,
                              fontFamily: 'UTM',
                              fontWeight: FontWeight.w700,
                              color: Color.fromRGBO( 51, 51, 51,0.5),
                              decoration: TextDecoration.lineThrough
                          )
                      )
                    ],
                  )
                ],
              ),
          ],
        ),
      ),
    );
  }
}
