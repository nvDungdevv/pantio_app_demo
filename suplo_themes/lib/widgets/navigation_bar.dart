

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:suplo_themes/screens/login/login_page.dart';
import 'package:suplo_themes/themes/c_icons.dart';
import 'package:suplo_themes/widgets/cart_icon.dart';

class NavigationBar extends StatelessWidget {
  NavigationBar({Key? key}) : super(key: key);
  final _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      centerTitle: true,
      backgroundColor: Colors.white,
      title: Padding(child: Image.asset("assets/images/logo.png"),padding: const EdgeInsets.only(top: 10),),
      actions: [
        IconButton(
              onPressed: ()async{
      await _auth.signOut();
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
        },
              icon: const Icon(Icons.search ,color: Colors.black,size: 30)),
        const Padding(
            padding: EdgeInsets.only(right: 16),
            child: CartIcon(),
        )

      ],
      leading: const Icon(Icons.clear_all,color: Colors.black,size: 30)
    );
  }
}
