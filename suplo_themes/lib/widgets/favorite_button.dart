
import 'package:flutter/material.dart';

class FavoriteButton extends StatefulWidget {
  const FavoriteButton({Key? key}) : super(key: key);

  @override
  _FavoriteButtonState createState() => _FavoriteButtonState();
}

class _FavoriteButtonState extends State<FavoriteButton> {
  bool favorite = false;
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon:  Icon(Icons.favorite_border,color: favorite?  Colors.red: const Color.fromRGBO( 51, 51, 51,0.5)),
      onPressed: () {
       setState(() {
         favorite=!favorite;
       });
      },
    );
  }
}
