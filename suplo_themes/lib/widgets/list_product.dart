import 'package:flutter/material.dart';
import 'package:suplo_themes/models/product.dart';
import 'items_list_product_horizontal.dart';
class ProductList extends StatelessWidget {
  const ProductList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

        return SizedBox(
          height: 180,
          child: ListView.builder(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              itemCount: FAKE_DATA_PRODUCT.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index){
                return ItemProductHorizontal( product: FAKE_DATA_PRODUCT[index]);
              }),
        );
  }
}
