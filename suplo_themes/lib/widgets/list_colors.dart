import 'package:flutter/material.dart';

class ListColors extends StatefulWidget {
   ListColors({Key? key}) : super(key: key);
    int? pickColor;
  @override
  _ListColorsState createState() => _ListColorsState();
}

class _ListColorsState extends State<ListColors> {
  @override
  Widget build(BuildContext context) {
    List<Color> listColors= [
      Colors.blue,
      Colors.amber,
      Colors.pink ,
      Colors.red,
      const Color.fromRGBO(191, 112, 84, 1),
      const Color.fromRGBO(114, 43, 2, 1),
      const Color.fromRGBO(216, 184, 184, 1),
      const Color.fromRGBO(176, 104, 248, 1),
      const Color.fromRGBO(254, 228, 36, 1),
      const Color.fromRGBO(254, 228, 36, 1),
      const Color.fromRGBO(254, 228, 36, 1)
    ];
    return Container(
      margin: const EdgeInsets.only(top:10,bottom: 20),
      height: 30,
      child: ListView.builder(
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          itemCount: listColors.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index){
            return InkWell(
              onTap: (){
                setState(() {
                  widget.pickColor=index;
                });
              },
              child: Container(
                margin: const EdgeInsets.only(right: 10),
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    border: Border.all(color: (widget.pickColor==index) ? Colors.black:Colors.transparent ,width: 1 ),
                    borderRadius: const BorderRadius.all(Radius.circular(30)),
                    color: listColors[index]
                ),
              ),
            );
          }),
    );
  }
}
