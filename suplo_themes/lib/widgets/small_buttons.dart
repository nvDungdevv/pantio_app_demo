import 'package:flutter/material.dart';

class SmallButton extends StatelessWidget{
  String nameOfButton;
  double heightOfButton;
  double minWidthOfButton;
  SmallButton({Key? key, required this.nameOfButton, required this.heightOfButton,required this.minWidthOfButton}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ButtonTheme(
        height: heightOfButton,
        minWidth: minWidthOfButton,
        child: OutlinedButton(
          style: OutlinedButton.styleFrom(
              backgroundColor: Colors.white,
              side: const BorderSide(color: Color.fromRGBO(51, 51, 51, 1)),
              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30)))
          ),
          onPressed: (){},
          child: Center(child:  Text(nameOfButton,style: const TextStyle(fontSize: 15, fontFamily: 'UTM', fontWeight: FontWeight.w400,color: Color.fromRGBO(51, 51, 51, 1)))),
        )
    );
  }

}