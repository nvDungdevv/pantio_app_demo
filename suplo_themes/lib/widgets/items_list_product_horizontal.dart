
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:suplo_themes/models/product.dart';
import 'package:suplo_themes/screens/product_detail_page.dart';

class ItemProductHorizontal extends StatefulWidget {
  ItemProductHorizontal({Key? key, required this.product}) : super(key: key);
  Product product;

  @override
  State<ItemProductHorizontal> createState() => _ItemProductHorizontalState();
}

class _ItemProductHorizontalState extends State<ItemProductHorizontal> {
  bool favorite= false;
  @override
  Widget build(BuildContext context) {
    var f = NumberFormat('###.0#', 'en_US');
    return InkWell(
      onTap: (){
        Navigator.pushNamed(context, ProductDetailPage.routeName);
      },
      child: Container(
        height: 259,
        width: 146,
        margin: const EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: const Color.fromRGBO( 161, 158, 158, 0.30196078431372547) ,width: 1),
          borderRadius: const BorderRadius.all(Radius.circular(8.0)),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Image.asset(widget.product.imageUrl!,fit: BoxFit.cover,height: 200, width: 146,),
                Positioned(
                    top:0,
                    right: 0,
                    child: IconButton(
                      icon: Icon(Icons.favorite_border,color: favorite?  Colors.red: const Color.fromRGBO( 51, 51, 51,0.5)),
                      onPressed: () {
                        setState(() {
                          favorite=!favorite;
                        });
                      },
                    )
                )
              ],
            ),
            Column(
              children: [
                Padding(padding: const EdgeInsets.only(top: 10,left: 8,right: 5),
                    child: Text(
                      widget.product.productName!,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(fontSize: 13, fontFamily: 'UTM'),
                    )
                ),
                Row(
                  children: [
                    Padding(padding:  const EdgeInsets.only(left: 8, right: 4),
                        child:Text(NumberFormat.currency(locale: 'eu', symbol: 'đ',decimalDigits: 0).format(widget.product.salePrice),style: const TextStyle(fontSize: 13, fontFamily: 'UTM', fontWeight: FontWeight.w700))
                    ),
                    Text(NumberFormat.currency(locale: 'eu', symbol: 'đ',decimalDigits: 0).format(widget.product.price),
                        style: const TextStyle(
                            fontSize: 10,
                            fontFamily: 'UTM',
                            fontWeight: FontWeight.w700,
                            color: Color.fromRGBO( 51, 51, 51,0.5),
                            decoration: TextDecoration.lineThrough
                        )
                    )
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
