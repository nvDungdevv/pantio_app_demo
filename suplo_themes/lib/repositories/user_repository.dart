
import 'dart:core';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class UserRepository {
 final FirebaseAuth _firebaseAuth;
 final GoogleSignIn _googleSignIn;
 UserRepository({FirebaseAuth? firebaseAuth, GoogleSignIn? googleSignIn}):
     _firebaseAuth= firebaseAuth ?? FirebaseAuth.instance,
     _googleSignIn= googleSignIn ?? GoogleSignIn();
 Future<UserCredential> signInWithPhoneNumberAndPassword(String phoneNumber, String password)async {
    return await _firebaseAuth.signInWithEmailAndPassword(email: phoneNumber.trim(), password: password);
 }
 Future<UserCredential> creatUserWithPhoneNumberAndPassword(String phoneNumber, String password)async{
  return await _firebaseAuth.createUserWithEmailAndPassword(email: phoneNumber, password: password);
 }
 Future<void>signOut() async{
   Future.wait([
    _firebaseAuth.signOut(),
    _googleSignIn.signOut()
   ]);
 }
 Future<bool> isSignedIn() async {
   return _firebaseAuth.currentUser != null;
 }
 User getUser()  {
  return  _firebaseAuth.currentUser!;
 }

}